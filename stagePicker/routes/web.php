<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/playMatch', function () {
    return view('playMatch');
});

Route::get('/pickStage', function () {
    return view('pickStage');
});

Route::get('/starterStrike', function () {
    return view('starterStrike');
});