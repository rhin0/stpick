@extends('layout')

@section('title')

Welcome

@endsection

@section('content')
<h1>
    <?php
    $stages = array('Battlefield',
        'Final Destination',
        'Pokémon Stadium 2',
        'Smashville',
        'Town and City');

    function starterStageStrike($s) {
        if (sizeof($s) == 1) {
            return key($s); /* Wrong value */
        } else {
            $ban = array_rand($s);
            $stage = $s[$ban];
            unset($s[$ban]);
            array_values($s);
            echo $stage . ' is banned. <br>';
            return starterStageStrike($s);
        }
    }

    $starterpick = starterStageStrike($stages);
    ?>
</h1>
<h1>   
    The counter-pick stage shall be {{ $stages[$starterpick] }}
</h1>

<?php?>


@endsection