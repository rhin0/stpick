<!doctype html>

@extends('layout')

@section('title')

Welcome

@endsection

@section('content')
<table align:center border="1" cellpadding="1" margin-left:auto margin-right:auto>
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td id="player1">0</td>
                    <td></td>
                    <td id="player2">0</td>
                </tr>
                <tr>

                    <td><input id="winPoint" class="score" type="button" value="+" onclick="button1(); winStage1();"   /></td>
                    <td><img id="image" src="{{url('/images/battlefield.jpg')}}" alt="Image" style=width:500px; min-width: 330px/>
                             <select id="StageList">
                            <option value="{{url('/images/battlefield.jpg')}}">Battlefield</option>
                            <option value="{{url('/images/final-destination.jpg')}}">Final Destination</option>
                            <option value="{{url('/images/town-and-city.jpg')}}">Town and City</option>
                            <option value="{{url('/images/pokemon-stadium-2.jpg')}}">Pokemon Stadium 2</option>
                            <option value="{{url('/images/smashville.jpg')}}">Smashville</option>
                            <option value="{{url('/images/kalos-pokemon-league.jpg')}}">Kalos Pokemon League</option>
                            <option value="{{url('/images/lylat-cruise.jpg')}}">Lylat Cruise</option>
                            <option value="{{url('/images/midgar.jpg')}}">Midgar</option>
                            <option value="{{url('/images/dream-land.jpg')}}">Dream Land</option>
                            <option value="{{url('/images/unova-pokemon-league.jpg')}}">Unova Pokemon League</option>
                            <option value="{{url('/images/yoshis-island-brawl.jpg')}}">Yoshi's Island</option>
                            <option value="{{url('/images/yoshis-story.jpg')}}">Yoshi's Story</option>
                        </select></td>
                    <td><input id="winPoint2" class="score" type="button" value="+" onclick="button2(); winStage2();"   /></td>
                </tr>
                <tr>
                    <td><div><img id="stageWon"  style="max-width:50%; max-height:25%" alt="No stage Won" /></div><div id="sname"></div></td>
                    <td><div></div></td>
                    <td><div><img id="stageWon2" style="max-width:50%; max-height:25%"  alt="No stage Won"/></div><div id="sname2"></div></td>
                </tr>
            </tbody>
            <script type="text/javascript">
                var x = 0;
                var y = 0;
                var winPoint = document.getElementById("player1");
                winPoint.innerHTML = x;
                
                 var winPoint2 = document.getElementById("player2");
                winPoint2.innerHTML = y;
                function button1() {
                    winPoint.innerHTML = ++x;
                }
                function button2() {
                    winPoint2.innerHTML = ++y;
                }
            </script>
        </table>
@endsection
