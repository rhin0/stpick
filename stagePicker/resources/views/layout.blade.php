<!DOCTYPE html>



<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script>
            function setStage() {
                var img = document.getElementById("image");
                img.src = this.value;
                return false;
            }
            function winStage1() {
                var img = document.getElementById("stageWon");
                var sw = document.getElementById("image");
                img.src = sw.src;
                return false;
            }
            
            function winStage2() {
                var img = document.getElementById("stageWon2");
                var sw = document.getElementById("image");
                img.src = sw.src;
                return false;
            }
            
            
            $(document).ready(function () {
                document.getElementById("StageList").onchange = setStage;
                
                
                
            });

        </script>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #663399;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }
            .score{
                background-color: #f5b041;
                border-radius: 10px;
                color: #fdfefe;
                padding: 10px;
                text-align: center;
                font-size: 22px; 
            }
            .content {
                text-align: center;
                font-size: 84px;
            }

            .title {
                font-size: 84px;
            }
            .table {
                font-size: 84px;

            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>


    @include('navbar')    		
    
    @yield('content')
    
    </body>
</html>
