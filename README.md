# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Project is intended to provide a website or web tool that can assist with matches that are played between players in a tournament setting for the game 'Super Smash Brothers'.
* Key features to include:
	* 	Emulate and automate the processes of a Smash Brothers match in a competitive environment
		* Starter Stages - set of allowable in-game battlefields that must be used exclusively for the first game of a match
		* Counter-pick Stages - set of allowable in-game battlefields in addition to the starter stages that can be used for all games after the first game of a match 
		* Stage Picking - Selecting an in-game battlefield for a game to take place 
		* Stage Striking - Eliminating one or more in-game battlefields as options for Stage Picking(above) 
		* Apply additional restrictions depending on previous stage that a player has won a game on(DSR ruling, modified DSR, none, etc.)
	* 	Select the set of allowable stages to be used for a competitive Smash Brothers game
	* 	Custom use for combinations of human vs human, human vs. computer, or computer vs. computer matches
	*   Developer Wish-list- Track the number of games won in a competitive match between two players from start to finish
	*	Alternative interface including the above features, including more features that tournament organizers and staff can use  
* Primary reference data include stage data and tournament data on the internet for the video game 'Super Smash Brothers Ultimate' on the Nintendo Switch
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Ryan Ocampo; email: oromeo10@gmail.com
* Other community or team contact