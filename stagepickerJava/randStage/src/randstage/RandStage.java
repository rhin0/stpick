/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package randstage;

import java.util.Random;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Ryan
 */
public class RandStage {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Random Ran =  new Random();
        int pick = (Ran.nextInt(5))+ 1;
        List<String> all= new ArrayList<String>(){{ 
            add("Battlefield"); 
            add("Final Destination");
            add("Pokémon Stadium 2"); 
            add("Smashville");
            add("Town and City"); 
            add("Yoshi’s Story");
            add("Lylat Cruise"); 
            add("Unova Pokémon League");
            add("Kalos Pokémon League"); 
            add("Yoshi's Island");
            add("Midgar"); 
            add("Dream Land");
            } }; 
        List<String> starter = new ArrayList<String>(){{ 
            add("Battlefield"); 
            add("Final Destination");
            add("Pokémon Stadium 2"); 
            add("Smashville");
            add("Town and City"); 
            } }; 

        
        

     
        //System.out.println(cpuStarterStageStrike(starter));
        
        //cpus game 2, game 3 if 2-0
        System.out.println(cpuCounterPick2(all));
        //System.out.println(all.toString());
        //cpus game 3 if 1-1, and on
        //System.out.println(cpuCounterPick(all,3));
        //System.out.println(all.toString());
        
        //HUMAN V CPU GAME 1
        //HUMAN STRIKES FIRST
        //System.out.println(humanStrikeStarter(starter,0));
        
        //CPU STRIKES FIRST
        //RUN LINE 1 FOR INPUT TO LINE 2
        //LINE 10
        //System.out.println(cpuStrikeStarter(starter));
        //LINE 2
        //System.out.println(humanStrikeSecond(starter,0,3,4));
        
        //game 2 human picks
        //System.out.println(humanCounterPickCPU2(all));
        
        //game 2 cpu picks
        //System.out.println(cpuCounterPickHuman2(all, 1,4));
        
        //game 3 human picks and on
        //System.out.println(humanCounterPickCPU(all,6));
        
        //game 3 cpu picks and on
        //System.out.println(cpuCounterPickHuman(all, 0,3,4));

        
        
    }
    public static String cpuStarterStageStrike(List<String> stage){
        Random r = new Random();
        int strike = r.nextInt(stage.size());
        if(stage.size() == 1){
        return "The counter-pick stage shall be " +stage.get(0);
        }else{
            String ban = stage.get(strike);
            stage.remove(stage.get(strike));
            return ban + " is banned. \n" +cpuStarterStageStrike(stage);
            
        }
        
        
    }
    public static String cpuCounterPick2(List<String> stage){
        Random r =  new Random();
        int strike = r.nextInt(stage.size());
        //ban 1
        String ban1 = stage.get(strike);
        stage.remove(stage.get(strike));
        //ban 2
        
        strike = r.nextInt(stage.size());
        String ban2 = stage.get(strike);
        stage.remove(stage.get(strike));
        //loser's pick
        
        return ban1 + " is banned. \n"+ ban2 + " is banned. \n" +"The counter-pick stage shall be "+stage.get(r.nextInt(stage.size()));
    }

    /**
     *
     * @param String
     * @param dsr
     */
    public static String cpuCounterPick(List<String> stage, int dsr){
        //dsr 
        String dave = stage.get(dsr);
        stage.remove(stage.get(dsr));
        return dave+ " is banned. \n"+cpuCounterPick2(stage);
    }
    public static String humanCounterPickCPU2(List<String> stage){
        Random r =  new Random();
        int strike = r.nextInt(stage.size());
        //ban 1
        String ban1 =  stage.get(strike) + " is banned";
        stage.remove(stage.get(strike));
        //ban 2
        strike = r.nextInt(stage.size());
        String ban2 = stage.get(strike)+ " is banned";
        stage.remove(stage.get(strike));
        
        return ban1 + "\n" + ban2 + "\n"+ "Please pick your stage. \n"+stage.toString();
    }
    public static String humanCounterPickCPU(List<String> stage,int dsr){
        
        String dave =  stage.get(dsr) + " is banned";
        stage.remove(stage.get(dsr));
        
        return dave+ "\n"+humanCounterPickCPU2(stage);
    }
    public static String cpuCounterPickHuman(List<String> stage,int ban1, int ban2, int dsr){
        Random r = new Random();
        String dave =  stage.get(dsr) + " is banned";
        
        
        //get stage bans' indices before dsr removal
        String firstBan = stage.get(ban1);
        String secondBan = stage.get(ban2);
        stage.remove(stage.get(dsr));
        
        return dave +"\n"+ cpuCounterPickHuman2(stage, stage.indexOf(firstBan), stage.indexOf(secondBan));
        
        
        
    }
    public static String cpuCounterPickHuman2(List<String> stage,int ban1, int ban2){
        Random r = new Random();
       
        String firstBan = stage.get(ban1);
        String secondBan = stage.get(ban2);
        
        stage.remove(stage.get(stage.indexOf(firstBan)));
        stage.remove(stage.get(stage.indexOf(secondBan)));
        
        return  firstBan + " is banned \n"+ secondBan + " is banned \n" +"Opponent picks "+ stage.get(r.nextInt(stage.size()));
        
        
        
    }
    
    public static String humanStrikeStarter(List<String> stage,int ban1){
        Random r = new Random();
       
        String firstStrike = stage.get(ban1);
        stage.remove(stage.get(ban1));
       
        int strike = r.nextInt(stage.size());
        //ban 1
        String strike2 =  stage.get(strike);
        stage.remove(stage.get(strike));
        //ban 2
        strike = r.nextInt(stage.size());
        String strike3 = stage.get(strike);
        stage.remove(stage.get(strike));
        
        return  firstStrike + " is banned \n"+ strike2 + " is banned \n" +strike3 + " is banned  \n" +"Please strike one stage \n"+ stage.toString();
        
        
    }
    
    public static String cpuStrikeStarter(List<String> stage){
        Random r = new Random();
        int strike = r.nextInt(stage.size());
        //ban 1
        String strike1 =  "Opponent strikes " + stage.get(strike) +" ("+"stage number " + stage.indexOf(stage.get(strike)) +")";
        stage.remove(stage.get(strike));
        return strike1+ "\n Please strike 2 other stages ";
    }
    
    public static String humanStrikeSecond(List<String> stage, int ban1, int ban2, int ban3){
        Random r = new Random();
        String firstStrike = stage.get(ban1);
        String secondStrike = stage.get(ban2);
        String thirdStrike= stage.get(ban3);
        
        stage.remove(stage.get(ban1));
        stage.remove(stage.get(stage.indexOf(secondStrike)));
        stage.remove(stage.get(stage.indexOf(thirdStrike)));
        int strike = r.nextInt(stage.size());
        //ban 1
        String strike4 =  stage.get(strike)+" is banned \n";
        stage.remove(stage.get(strike));
        return  firstStrike+ " is banned \n" +secondStrike + " is banned \n"+thirdStrike + " is banned \n" +strike4 + "Starting stage is " +stage.get(0);
    }
    
    
    
}
